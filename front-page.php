<?php
/**
 * Template Name: Home Page
 *
 * @package square
 */

$square_page = '';
$square_page_array = get_pages();
if(is_array($square_page_array)){
	$square_page = $square_page_array[0]->ID;
}

if ( 'page' == get_option( 'show_on_front' ) ) {
    include( get_page_template() );
}else{
get_header(); 
?>
<section id="sq-home-slider-section">
	<div id="sq-bx-slider">
	<?php for ($i=1; $i < 4; $i++) {  
		if($i == 1){
			$square_slider_title = get_theme_mod('square_slider_title1', __('Free WordPress Themes', 'square'));
			$square_slider_subtitle = get_theme_mod('square_slider_subtitle1', __('Create website in no time', 'square'));
			$square_slider_image = get_theme_mod('square_slider_image1', get_template_directory_uri().'/images/bg.jpg');
		}else{
			$square_slider_title = get_theme_mod('square_slider_title'.$i);
			$square_slider_subtitle = get_theme_mod('square_slider_subtitle'.$i);
			$square_slider_image = get_theme_mod('square_slider_image'.$i);
		}

		if( $square_slider_image ){
		?>
		<div class="sq-slide sq-slide-count<?php echo $i; ?>">
			<img src="<?php echo esc_url( $square_slider_image ); ?>">
			
			<?php if( $square_slider_title || $square_slider_subtitle){ ?>
				<div class="sq-slide-caption">
					<img src="/wp-content/uploads/2016/03/logo-white.png">
					<!-- <div class="sq-slide-cap-title animated fadeInDown">
						<?php // echo esc_html( $square_slider_title ); ?>
					</div>

					<div class="sq-slide-cap-desc animated fadeInUp">
						<?php // echo esc_html( $square_slider_subtitle ); ?>
					</div> -->
				</div>
			<?php } ?>
		</div>
	<?php 
		}
	} ?>
	</div>
	<div class="sq-banner-shadow"><img src="<?php echo get_template_directory_uri() ?>/images/banner-shadow.png"></div>
</section>

<section id="sq-about-us-section" class="sq-section">
	<div class="sq-container sq-clearfix">
		<div class="sq-about-sec">
		<?php 
			$args = array(
				'page_id' => get_theme_mod('square_about_page') 
				);
			$query = new WP_Query($args);
			if($query->have_posts() && get_theme_mod('square_about_page')):
				while($query->have_posts()) : $query->the_post();
			?>
			<h2 class="sq-section-title"><?php the_title(); ?></h2>
			<div class="sq-content"><?php the_content(); ?></div>
			<?php
			endwhile;
			endif;	
			wp_reset_postdata();
		?>
		</div>
	</div>
</section>

<section id="sq-tab-section" class="sq-section">
	<div class="sq-container sq-clearfix">
		<ul class="sq-tab">
			<?php 
				for( $i = 1; $i < 6; $i++ ){
					$square_tab_title = get_theme_mod('square_tab_title'.$i);
					$square_tab_icon = get_theme_mod('square_tab_icon'.$i);
					
					if($square_tab_title){
					?>
					<li class="sq-tab-list<?php echo $i; ?>">
					<a href="#<?php echo 'sq-tab'.$i; ?>">
					<?php echo '<i class="fa '.esc_attr( $square_tab_icon ).'"></i><span>'.esc_html( $square_tab_title ) .'</span>'; ?>
					</a>
					</li>
				<?php
					}
				}
			?>
		</ul>

		<div class="sq-tab-content">
			<?php 
				for ($i = 1; $i < 6 ; $i++) { 
					$square_tab_page = get_theme_mod('square_tab_page'.$i);
					if($square_tab_page){
					?>
						<div class="sq-tab-pane animated zoomIn" id="<?php echo 'sq-tab'.$i; ?>">
							<?php
							$args = array(
								'page_id' => $square_tab_page
								);
							$query = new WP_Query($args);
							if($query->have_posts()):
								while($query->have_posts()) : $query->the_post();
							?>
							<a href="<?php the_permalink(); ?>">
								<h2 class="sq-section-title"><?php the_title(); ?></h2>
							</a>
							<div class="sq-content">
								<?php the_content(); ?>
							</div>
							<?php
								endwhile;
							endif;	
							wp_reset_postdata();
							?>
						</div>
					<?php
					}
				}
			?>
		</div>
	</div>
</section>

<section id="tj-posts">
	<h2 class="sq-section-title">Most Recent Post:</h2>

	<?php 
		if ( have_posts() ) {

			the_post();
			get_template_part( 'template-parts/content', get_post_format() );
			kriesi_pagination();

		} else {

			get_template_part( 'template-parts/content', 'none' );

		} 
	?>
</section>

<?php 
get_footer(); 
} 